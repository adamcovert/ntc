$(document).ready(function() {

  var inputs = document.querySelectorAll( '.s-field-file__input:not([disabled])' );
  Array.prototype.forEach.call( inputs, function( input )
  {
    var label  = closest(input, '.s-field-file').querySelector( '.s-field-file__name-text' ),
        labelVal = label.innerHTML;

    input.addEventListener( 'change', function( e ) {
      var fileName = '';
      if( this.files && this.files.length > 1 ) {
        fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
      }
      else {
        fileName = e.target.value.split( '\\' ).pop();
      }

      if( fileName ) {
        label.innerHTML = fileName;
      }
      else {
        label.innerHTML = labelVal;
      }
    });
  });

  $('.s-side-nav__toggle').on('click', function() {
    if ($(this).next().hasClass('s-side-nav__list')) {
      $(this).next().toggleClass('s-side-nav__list--is-open')
    } else {
      return false;
    }
  });

  $(document).on('click', function(e) {
    if ($(e.target).hasClass('s-page-header__language-toggler')) {
      $('.s-page-header__language').toggleClass('s-page-header__language--is-open')
    } else {
      $('.s-page-header__language').removeClass('s-page-header__language--is-open')
    }
  });

  var promoSwiper = new Swiper('.s-slider--promo', {
    slidesPerView: 1
  });

  var menuSwiper = new Swiper('.s-slider--menu', {
    slidesPerView: 3,
    spaceBetween: 10,
    speed: 1000,
    navigation: {
      nextEl: '.s-slider__button--next',
      prevEl: '.s-slider__button--prev',
    },
    breakpointsInverse: true,
    breakpoints: {
      320: {
        slidesPerView: 1,
      },
      540: {
        slidesPerView: 2,
      },
      992: {
        slidesPerView: 3,
      }
    }
  });

  var categorySwiper = new Swiper('.s-slider--category', {
    slidesPerView: 1,
    spaceBetween: 10,
    speed: 1000,
    navigation: {
      nextEl: '.s-slider__button--next',
      prevEl: '.s-slider__button--prev',
    },
    breakpointsInverse: true,
    breakpoints: {
      320: {
        slidesPerView: 2,
      },
      425: {
        slidesPerView: 3,
      },
      768: {
        slidesPerView: 3,
      },
      992: {
        slidesPerView: 4,
      }
    }
  });

  var partnersSwiper = new Swiper('.s-slider--partners', {
    slidesPerView: 6,
    spaceBetween: 10,
    speed: 1000,
    autoplay: {
      delay: 5000,
    },
    breakpointsInverse: true,
    breakpoints: {
      320: {
        slidesPerView: 2,
      },
      425: {
        slidesPerView: 4,
      },
      768: {
        slidesPerView: 6,
      }
    }
  });

  var productsSwiper = new Swiper('.s-slider--products', {
    slidesPerView: 4,
    spaceBetween: 10,
    speed: 1000,
    autoplay: {
      delay: 5000,
    },
    navigation: {
      nextEl: '.s-slider__button--next',
      prevEl: '.s-slider__button--prev',
    },
    breakpointsInverse: true,
    breakpoints: {
      320: {
        slidesPerView: 2,
      },
      425: {
        slidesPerView: 3,
      },
      768: {
        slidesPerView: 4,
      }
    }
  });

  var certificateSwiper = new Swiper('.s-slider--certificates', {
    slidesPerView: 4,
    spaceBetween: 10,
    speed: 1000,
    autoplay: {
      delay: 5000,
    },
    navigation: {
      nextEl: '.s-slider__button--next',
      prevEl: '.s-slider__button--prev',
    },
    breakpointsInverse: true,
    breakpoints: {
      320: {
        slidesPerView: 1,
      },
      425: {
        slidesPerView: 2,
      },
      768: {
        slidesPerView: 4,
      }
    }
  });

  var certificateSwiper = new Swiper('.s-slider--special-offer', {
    slidesPerView: 4,
    spaceBetween: 10,
    speed: 1000,
    autoplay: {
      delay: 5000,
    },
    navigation: {
      nextEl: '.s-slider__button--next',
      prevEl: '.s-slider__button--prev',
    },
    breakpointsInverse: true,
    breakpoints: {
      320: {
        slidesPerView: 1,
      },
      425: {
        slidesPerView: 2,
      },
      768: {
        slidesPerView: 3,
      }
    }
  });
});